<?php
  // Variables necesarias para lograr la conexión a la base de datos
  $host     = '127.0.0.1';
  $database = 'notesclub';
  $user     = 'root';
  $password = '';
  $charset  = 'utf8';
  // Opciones relacionadas al PDO
  $options = [
    PDO::ATTR_ERRMODE             => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE  => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES    => false,
  ];
  // Conexión a la base de datos
  $dsn = "mysql:host=$host;dbname=$database;charset=$charset";
  // Creamos nuestro objeto PDO en donde se almacenará la conexión a la base datos.
  try {
    $pdo = new PDO($dsn, $user, $password, $options);
  } catch (\PDOException $error) {
    throw new \PDOException($error->getMessage(), (int)$error->getCode());
  }
?>