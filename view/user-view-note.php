<?php
  include "../controladores/conexion.php";
  require_once "../controladores/obtener_usuario.php";
  $row = getUser($conn);
  if (!$row) {
    ?>
      <script>
        alert("Está intentando acceder sin autorización!");
        window.location.replace("../index.html");
      </script>
    <?php
  } else {
    // Tratamos de obtener el ID de la nota a visualizar:
    if (isset($_GET["id"]) && !empty($_GET["id"])) {
      $contenido = mysqli_query($conn, "SELECT * FROM notas WHERE id_nota='$_GET[id]'");
      if (mysqli_num_rows($contenido) === 1) {
        $nota = mysqli_fetch_assoc($contenido);
      }
    } else {
      ?>
      <script>
        alert("No se pudo encontrar la nota a visualizar...");
        window.location.replace("user-overview.php");
      </script>
      <?php
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Ver nota</title>
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css" />
  <link rel="stylesheet" href="../css/open-iconic-bootstrap.min.css">
  <link rel="stylesheet" href="../css/quill.snow.css">
  <link rel="Shortcut Icon" href="../favicon.ico" type="image/x-icon" />
</head>
<body class="navbar-separator">
  <!-- #################### Navbar #################### -->
  <?php require '../templates/navbar.php'; ?>
  
  <!-- #################### Contenido Principal #################### -->
  <section class="separator">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-10">

        <div id="noteContainer">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-auto mr-auto mt-1">
                    <h5 class="mb-0"><?php echo $nota['titulo']; ?></h5>
                  </div>
                  <div class="col-auto pr-1 pl-1">
                    <span data-toggle="modal" data-target="#confirmModal" data-note-title="<?php echo $nota['titulo']; ?>" data-note-id="<?php echo $nota['id_nota']; ?>">
                      <a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar"><span class="oi oi-trash"></span></a></span>
                  </div>
                  <?php if ($nota['compartir'] == 1): ?>
                    <div class="col-auto pl-1 pr-1">
                      <a href="user-shared-note.php?id=<?php echo $nota['id_nota']; ?>" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Informacion..."><span class="oi oi-people"></span></a>      
                    </div>
                    <?php elseif ($nota['autor'] == $_SESSION['id'] && $_SESSION['tipo'] == 3): ?>
                    <div class="col-auto px-1">
                      <span data-toggle="modal" data-target="#shareModal" data-note-title="<?php echo $nota['titulo']; ?>">
                        <a href="#" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Compartir nota"><span class="oi oi-share-boxed oi-align-center"></span></a>
                      </span>
                    </div>
                    <?php endif?>
                  <?php if ($nota['edicion'] == 1 || $nota['autor'] == $_SESSION['id']): ?>
                    <?php $user_type = $_SESSION['id'] == 3 ? "user-edit-note.php" : "user-edit-notice.php"; ?>
                    <div class="col-auto pl-1 pr-1">
                      <a href="<?php echo $user_type?>?id=<?php echo $nota['id_nota']; ?>" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Editar"><span class="oi oi-pencil"></span></a>
                    </div>
                  <?php endif?>
                  <div class="col-auto pl-1">
                      <a href="user-overview.php" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Regresar"><span class="oi oi-arrow-left"></span></a>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <h5>Etiquetas:</h5>
                    <div id="tags" data-note-id="<?php echo $nota['id_nota']; ?>">
                      <p id="tagsOutput"></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <h5>Fecha de modificacion:</h5>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col">
                    <?php echo utf8_encode($nota['contenido']); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>

  <!-- #################### Modals #################### -->
  <?php include '../templates/modal.php'; ?>
  <?php include '../templates/shareModal.php'; ?>

  <!-- Javascript files -->
  <script src="../js/jquery-3.3.1.slim.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <!-- Bootstrap's navbar dependency -->
  <script src="../bootstrap/js/util.js"></script>
  <script src="../bootstrap/js/collapse.js"></script>
  <script src="../bootstrap/js/dropdown.js"></script>
  <script src="../bootstrap/js/modal.js"></script>
  <script src="../bootstrap/js/tooltip.js"></script>
  <script>
    // Pequeño script para activar los tooltips:
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });

    // Aqui manejamos si la nota tiene etiquetas o no
    const tagsContainer = document.querySelector('#tags');
    const noteId = tagsContainer.dataset.noteId;
    // console.log(noteId);
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `../controladores/api_tags.php?idnota=${noteId}`, true);
    xhr.onload = function () {
      if (xhr.status === 200) {
        // Si se acepto la respuesta
        let output = '';
        const tags = JSON.parse(this.responseText);
        tags.forEach(function (tag) {
          output += `${tag}, `;
        });
        console.log(tags);
        document.querySelector('#tagsOutput').innerText = output;
      } else {
        alert('Hubo un error al retornar las etiquetas...');
      }
    }
    // Enviamos la solicitud
    xhr.send();

    // Aquí manejamos los modales:
    // Primer modal - Confirmar al eliminar una nota
    $('#confirmModal').on('show.bs.modal', function (event) {
      //Cambiamos el contenido del modal:
      var modal = $(this);
      var span = $(event.relatedTarget);
      const noteId = span.data('noteId');
      const noteTitle = span.data('noteTitle');
      modal.find('.modal-title').text(`Eliminar "${noteTitle}"`);
      modal.find('.modal-body').text('¿Estás seguro de eliminar esta nota?');
      const confirm = document.querySelector('#confirmModal button.btn-danger');
      confirm.addEventListener('click', function (e) {
        // Creamos un AJAX y mandamos un POST que elimine la nota:
        const xhr = new XMLHttpRequest();
        const values = `id_nota=${noteId}`;
        xhr.open('POST', '../controladores/eliminar_nota.php', true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        xhr.onload = function () {
          if (xhr.status === 200) {
            alert('Nota eliminada con éxito!');
          } else {
            alert('Ocurrió un error...');
          }
        };
        xhr.send(values);
        $('#confirmModal').modal('toggle');
        window.location.reload();
      });
    });

    // Modal para compartir:
    $('#shareModal').on('show.bs.modal', function (event) {
      var modal = $(this);
      var span = $(event.relatedTarget);
      // const noteId = span.data('noteId');
      const noteTitle = span.data('noteTitle');
      modal.find('.modal-title').text(`Compartir "${noteTitle}"`);
      modal.find('.btn-primary').bind('click', function () {
        // Creamos un AJAX y mandamos un POST que elimine la nota:
        const user = modal.find('input[name="usuario"]').val();
        const origin = modal.find('input[name="origen"]').val();
        const values = `nombre=${noteTitle}&usuario=${user}&origen=${origin}`;
        const xhr = new XMLHttpRequest();
        console.log(values);
        xhr.open('POST', '../controladores/compartir_nota.php', true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        xhr.onload = function () {
          if (xhr.status === 200) {
            alert('Nota compartida con éxito!');
          } else {
            alert('Ocurrió un error...');
          }
        };
        xhr.send(values);
        $('#shareModal').modal('toggle');
        window.location.reload();
      });
    });
  </script>
</body>
</html>