<?php 
  // header('Content-Type: text/html; charset=ISO-8859-1');
  include "../controladores/conexion.php";
  require_once "../controladores/obtener_usuario.php";
  $row = getUser($conn);
  if (!$row) {
    ?>
      <script>
        alert("Está intentando acceder sin autorización!");
        window.location.replace("../index.html");
      </script>
    <?php
  } else {
    // Traemos todas las notas asociadas
    $notas = mysqli_query($conn, "SELECT * FROM notas WHERE autor='$_SESSION[id]' ORDER BY  fecha_modificacion DESC LIMIT 5");
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Página principal</title>
  <link rel="stylesheet" href="../css/open-iconic-bootstrap.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css" />
  <link rel="Shortcut Icon" href="../favicon.ico" type="image/x-icon" />
</head>
<body class="navbar-separator bg-light">
  <!-- #################### Navbar #################### -->
  <?php require '../templates/navbar.php'; ?>

  <!-- #################### Contenido principal #################### -->
  <section class="separator">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-10">
          <div class="card">
            <div class="card-body">
              <h3>Bienvenido, <?php echo $row["nombre"]; ?></h3>
              <hr>

              <?php if ($row['tipo'] == 3) : ?>

              <form action="#">
                <div class="form-row mb-3">
                  <div class="col-md-12">
                    <input type="text" class="form-control" name="search-input" id="searchInput" placeholder="Buscar una nota...">
                  </div>
                </div>  
              </form>
              
              <form action="#">
                <div class="form-row mb-3">
                  <div class="col-auto mr-auto mt-1">
                  <h5 class="mb-0">Notas recientes</h5>
                  </div>
                  <div class="col-auto">
                    <a href="user-new-note.php" class="btn btn-success"><span class="oi oi-document"></span> Crear nota</a>
                  </div>
                </div>
              </form>

              <div id="notesContainer">
                <?php while ($nota = mysqli_fetch_assoc($notas)): ?>
                  <div class="card mt-3">
                      <div class="card-body">
                        <div class="row">
                          <div class="col-auto mr-auto mt-1">
                            <h5 class="mb-0"><a href="user-view-note.php<?php echo '?id='.$nota['id_nota']; ?>"><?php echo $nota['titulo'] ?></a></h5>
                          </div>
                          <div class="col-auto pl-1 pr-1">
                            <span data-toggle="modal" data-target="#confirmModal" data-note-title="<?php echo $nota['titulo']; ?>" data-note-id="<?php echo $nota['id_nota']; ?>">
                              <button type="button" href="#" class="delete-note btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar nota"><span class="oi oi-trash oi-align-center"></span></button>
                            </span>
                          </div>
                          <div class="col-auto pl-1 pr-1">
                            <a href="user-edit-note.php?id=<?php echo $nota['id_nota']; ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar nota"><span class="oi oi-pencil oi-align-center"></span></a>
                          </div>
                          <div class="col-auto pl-1">
                            <?php if ($nota['compartir'] == 1): ?>
                              <a href="user-shared-note.php?id=<?php echo $nota['id_nota']; ?>" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Informacion..."><span class="oi oi-people"></span></a>      
                            <?php else : ?>
                              <span data-toggle="modal" data-target="#shareModal" data-note-title="<?php echo $nota['titulo']; ?>">
                                <a href="#" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Compartir nota"><span class="oi oi-share-boxed oi-align-center"></span></a>
                              </span>
                            <?php endif?>
                          </div>
                        </div>
                      <hr>
                      <div class="row">
                        <div class="col">
                          <?php if (strlen($nota['contenido']) > 100): ?>
                            <?php echo mb_substr(utf8_encode($nota['contenido']), 0, 100, 'UTF-8') . '...'; ?>
                          <?php else: ?>
                            <?php echo utf8_encode($nota['contenido']); ?>
                          <?php endif?>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endwhile ?>
              </div>

              <?php elseif ($row['tipo'] == 2): ?>
                <!-- TODO -->
                <form action="#">
                  <div class="form-row mb-3">
                    <div class="col">
                      <a href="user-new-notice.php" class="btn btn-lg btn-success btn-block"><span class="oi oi-document"></span> Crear aviso</a>
                    </div>
                  </div>
                </form>

                <div class="row justify-content-center">
                  <div class="col">
                    <a href="user-notes.php" class="btn btn-lg btn-primary btn-block">Avisos</a>
                  </div>
                  <div class="col">
                    <a href="user-shared-groups.php" class="btn btn-lg btn-primary btn-block">Grupos</a>
                  </div>
                </div>
              <?php endif ?>
              
            </div> <!-- end of main div.card-body -->
          </div> <!-- end of main div.card -->
        </div> <!-- end of div.col-md-10 -->
      </div> <!-- end of div.row -->
    </div> <!-- end of div.container-fluid -->
  </section>

  <!-- #################### Modals #################### -->
  <?php include '../templates/modal.php'; ?>
  <?php include '../templates/shareModal.php'; ?>

  <!-- #################### Footer #################### -->
  <footer class="page-footer">
    <div class="footer-copyright">&#x00a9; 2018. NotesClub, Inc.</div>
  </footer>

  <!-- Javascript files -->
  <script src="../js/jquery-3.3.1.slim.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <!-- Bootstrap's navbar dependency -->
  <script src="../bootstrap/js/util.js"></script>
  <script src="../bootstrap/js/collapse.js"></script>
  <script src="../bootstrap/js/dropdown.js"></script>
  <!-- end of navbar dependencies -->
  <script src="../bootstrap/js/modal.js"></script>
  <script src="../bootstrap/js/tooltip.js"></script>
  <script>
    // Pequeño script para activar los tooltips:
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });
    // Pequeño script para cambiar el contenido del modal:
    $('#confirmModal').on('show.bs.modal', function (event) {
      //Cambiamos el contenido del modal:
      var modal = $(this);
      var span = $(event.relatedTarget);
      const noteId = span.data('noteId');
      const noteTitle = span.data('noteTitle');
      modal.find('.modal-title').text(`Eliminar "${noteTitle}"`);
      modal.find('.modal-body').text('¿Estás seguro de eliminar esta nota?');
      modal.find('.btn-danger').bind('click', function (e) {
        // Creamos un AJAX y mandamos un POST que elimine la nota:
        const xhr = new XMLHttpRequest();
        const values = `id_nota=${noteId}`;
        xhr.open('POST', '../controladores/eliminar_nota.php', true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        xhr.onload = function () {
          if (xhr.status === 200) {
            alert('Nota eliminada con éxito!');
          } else {
            alert('Ocurrió un error...');
          }
        };
        xhr.send(values);
        $('#confirmModal').modal('toggle');
        window.location.reload();
      });
    });
    // Modal para compartir:
    $('#shareModal').on('show.bs.modal', function (event) {
      var modal = $(this);
      var span = $(event.relatedTarget);
      // const noteId = span.data('noteId');
      const noteTitle = span.data('noteTitle');
      modal.find('.modal-title').text(`Compartir "${noteTitle}"`);
      modal.find('.btn-primary').bind('click', function () {
        // Creamos un AJAX y mandamos un POST que elimine la nota:
        const user = modal.find('input[name="usuario"]').val();
        const origin = modal.find('input[name="origen"]').val();
        const values = `nombre=${noteTitle}&usuario=${user}&origen=${origin}`;
        const xhr = new XMLHttpRequest();
        console.log(values);
        xhr.open('POST', '../controladores/compartir_nota.php', true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        xhr.onload = function () {
          if (xhr.status === 200) {
            alert('Nota compartida con éxito!');
          } else {
            alert('Ocurrió un error...');
          }
        };
        xhr.send(values);
        $('#shareModal').modal('toggle');
        window.location.reload();
      });
    });
  </script>
</body>
</html>