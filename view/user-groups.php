<?php 
  // Se reanuda la sesión actual
  session_start();
  // Comprobamos si existe una sesión, en dado caso que no, botamos a la página de inicio
  if (isset($_SESSION['id']) && !empty($_SESSION['id']))
  {
    //conexion a la base de datos 
    include "../controladores/conexion.php";
    // trae de la base de datos el nombre del alumno
    $consulta = "SELECT
                  nombre, tipo
                FROM
                  usuarios
                WHERE
                  id_user='$_SESSION[id]'";
    // $resultado = mysqli_query($conn,"SELECT nombre FROM usuarios WHERE id_user='$_SESSION[id]' ");
    $resultado = mysqli_query($conn, $consulta);
    if (mysqli_num_rows($resultado) > 0)  {
        // output data of each row
        $row = mysqli_fetch_assoc($resultado);
    }
    // Notificamos los errores, a excepción de E_NOTICE
    error_reporting(E_ALL ^ E_NOTICE);
    // Traemos todas las notas asociadas
    $id_nota = $_POST['id_nota'];
    $nota = mysqli_query($conn, "SELECT * FROM notas WHERE id_nota='$id_nota'");
    if (mysqli_num_rows($nota) == 1)
    {
      $line = mysqli_fetch_assoc($nota);
    }
  }
  else {
    ?>
    <script>
      alert("Está intentando acceder sin autorización!");
			window.location.replace("../index.html");
		</script>
		<?php
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Grupos</title>
  <link rel="stylesheet" href="../css/open-iconic-bootstrap.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css" />
  <link rel="Shortcut Icon" href="../favicon.ico" type="image/x-icon" />
</head>
<body class="navbar-separator bg-light">
  <!-- #################### Navbar #################### -->
  <?php require '../templates/navbar.php' ?>

  <!-- #################### Main content #################### -->
  <section class="separator">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-auto mr-auto mt-1">
                  <h5>Grupos</h5>
                </div>
                <div class="col-auto ml'1">
                    <a href="user-overview.php" class="btn btn-primary"><span class="oi oi-arrow-left mr-1"></span>Regresar</a>
                </div>
              </div>
            </div>
            <div class="card-body">
              <table class="table table-responsive-md table-stripped">
                <thead>
                  <tr>
                    <th>Grupo</th>
                    <th>Nota</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>3M_BD</td>
                    <td><a href="#">Una nota de prueba</a></td>
                  </tr>
                  <tr>
                    <td>3M_desarrollo</td>
                    <td><a href="#">Otra nota mas de prueba</a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- #################### Footer #################### -->
  <footer class="page-footer">
    <div class="footer-copyright">&#x00a9; 2018. NotesClub, Inc.</div>
  </footer>

  <!-- Javascript files -->
  <script src="../js/jquery-3.3.1.slim.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <!-- Bootstrap's navbar dependency -->
  <script src="../bootstrap/js/util.js"></script>
  <script src="../bootstrap/js/collapse.js"></script>
  <script src="../bootstrap/js/dropdown.js"></script>
  <!-- end of navbar dependencies -->
</body>
</html>