<?php 
  // Se reanuda la sesión actual
  session_start();
  // Comprobamos si existe una sesión, en dado caso que no, botamos a la página de inicio
  if (isset($_SESSION['id']) && !empty($_SESSION['id']))
  {
    //conexion a la base de datos 
    include "../controladores/conexion.php";
    // trae de la base de datos el nombre del alumno
    $consulta = "SELECT
                  nombre, tipo
                FROM
                  usuarios
                WHERE
                  id_user='$_SESSION[id]'";
    // $resultado = mysqli_query($conn,"SELECT nombre FROM usuarios WHERE id_user='$_SESSION[id]' ");
    $resultado = mysqli_query($conn, $consulta);
    if (mysqli_num_rows($resultado) > 0)  {
        // output data of each row
        $row = mysqli_fetch_assoc($resultado);
    }
    // Notificamos los errores, a excepción de E_NOTICE
    error_reporting(E_ALL ^ E_NOTICE);
    // Traemos todas las notas asociadas
    $id_nota = $_POST['id_nota'];
    $nota = mysqli_query($conn, "SELECT * FROM notas WHERE id_nota='$id_nota'");
    if (mysqli_num_rows($nota) == 1)
    {
      $line = mysqli_fetch_assoc($nota);
    }
  }
  else {
    ?>
    <script>
      alert("Está intentando acceder sin autorización!");
			window.location.replace("../index.html");
		</script>
		<?php
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Página principal</title>
  <link rel="stylesheet" href="../css/open-iconic-bootstrap.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css" />
  <link rel="Shortcut Icon" href="../favicon.ico" type="image/x-icon" />
</head>
<body class="navbar-separator bg-light">
  <!-- #################### Navbar #################### -->
  <?php require '../templates/navbar.php' ?>

  <!-- #################### Main content #################### -->
  <section class="separator">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-10">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-auto mr-auto">
                  <h5>Notas</h5>
                </div>
                <div class="col-auto pl-1">
                    <a href="user-overview.php" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Regresar"><span class="oi oi-arrow-left"></span></a>
                </div>
              </div>
            </div>
            <div class="card-body">
              <h5>Individuales</h5>
              <hr>
              <?php
                // Vemos si existen notas personales
                $notas = mysqli_query($conn,"SELECT id_nota, titulo FROM notas WHERE autor='$_SESSION[id]' AND compartir=false ORDER BY fecha_modificacion DESC");
              ?>
              <?php if (mysqli_num_rows($notas) > 0): ?>
                <?php while($nota = mysqli_fetch_assoc($notas)): ?>
                  <div class="card">
                    <div class="card-header p-2">
                      <div class="row">
                        <div class="col mt-sm-2 ml-1">
                          <h5><a href="user-view-note.php?id=<?php echo $nota['id_nota'];?>"><?php echo $nota['titulo']; ?></a></h5>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endwhile?>
              <?php else: ?>
                <div class="row">
                  <div class="col-auto mr-auto">
                    <p>Parece ser que no cuentas con alguna nota individual...</p>
                  </div>
                  <div class="col-auto">
                    <button href="user-new-note.php" class="btn btn-success"><span class="oi oi-file mr-1"></span> Crear una nota</button>
                  </div>
                </div>
              <?php endif?>
              <h5>Compartidas</h5>
              <hr>
              <?php 
              $grupos=mysqli_query($conn,"SELECT * FROM notas WHERE autor='$_SESSION[id]' AND compartir=true ORDER BY fecha_modificacion DESC");
              /*if (!$grupos) {
                echo mysqli_error($conn);
              }*/
              if (mysqli_num_rows($grupos)>0) 
              {
                while ($line = mysqli_fetch_assoc($grupos)) 
                {
                  echo '<li><a href="user-view-note.php?id='.$line['id_nota'].'">'.$line["titulo"]."</a></li>";
                }
              }
              ?>

              <h5>Compartidas contigo</h5>
              <?php 
              $sql_select=mysqli_query($conn,"SELECT * FROM notas WHERE id_nota in(SELECT id_nota FROM nota_usuario WHERE id_user='$_SESSION[id]') ORDER BY fecha_modificacion DESC");
              /*if (!$grupos) {
                echo mysqli_error($conn);
              }*/
              if (mysqli_num_rows($sql_select)>0) 
              {
                while ($linea = mysqli_fetch_assoc($sql_select)) 
                {
                  echo '<li><a href="user-view-note.php?id='.$linea['id_nota'].'">'.$linea["titulo"]."</a></li>";
                }
              }
            ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- #################### Footer #################### -->
  <footer class="page-footer">
    <div class="footer-copyright">&#x00a9; 2018. NotesClub, Inc.</div>
  </footer>

  <!-- Javascript files -->
  <script src="../js/jquery-3.3.1.slim.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <!-- Bootstrap's navbar dependency -->
  <script src="../bootstrap/js/util.js"></script>
  <script src="../bootstrap/js/collapse.js"></script>
  <script src="../bootstrap/js/dropdown.js"></script>
  <!-- end of navbar dependencies -->
  <script src="../bootstrap/js/tooltip.js"></script>
  <script>
    // Pequeño script para activar los tooltips:
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });
  </script>
</body>
</html>