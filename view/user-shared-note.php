<?php 
  require "../controladores/conexion.php";
  require_once "../controladores/obtener_usuario.php";
  $row = getUser($conn);
  if (!$row) {
    ?>
      <script>
        alert("Está intentando acceder sin autorización!");
        window.location.replace("../index.html");
      </script>
    <?php
  } else {
    // Tratamos de obtener el ID de la nota a visualizar:
    if (isset($_GET["id"]) && !empty($_GET["id"])) {
      $contenido = mysqli_query($conn, "SELECT * FROM notas WHERE id_nota='$_GET[id]'");
      if (mysqli_num_rows($contenido) === 1) {
        $nota = mysqli_fetch_assoc($contenido);
      }
    } else {
      ?>
      <script>
        alert("No se pudo encontrar la nota a visualizar...");
        window.location.replace("user-overview.php");
      </script>
      <?php
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Nota compartida</title>
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css" />
  <link rel="stylesheet" href="../css/open-iconic-bootstrap.min.css">
  <link rel="stylesheet" href="../css/quill.snow.css">
  <link rel="Shortcut Icon" href="../favicon.ico" type="image/x-icon" />
</head>
<body class="navbar-separator bg-light">
  <!-- #################### Navbar #################### -->
  <?php require '../templates/navbar.php' ?> 

  <!-- #################### Main Content #################### -->
  <section class="separator">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-10">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-auto mr-auto mt-1">
                  <h5>Nota compartida: <?php echo $nota['titulo']; ?></h5>
                </div>
                <?php if ($nota['autor'] == $_SESSION['id']): ?>
                  <div class="col-auto pr-1 pl-1">
                    <button type="submit" class="btn btn-sm btn-danger" form="eliminar" data-toggle="tooltip" data-placement="top" title="Eliminar"><span class="oi oi-trash"></span></button>
                    <input type="hidden" name="id_nota" value="<?php echo $nota['id_nota']; ?>" form="eliminar">
                    <input type="hidden" name="origen" value="<?php echo ($row['tipo'] == 3 ? "AN" : "AG"); ?>" form="eliminar">
                  </div>
                <?php else: ?>
                  <div class="col-auto pr-1 pl-1">
                    <button type="submit" class="btn btn-sm btn-danger" form="descompartir" data-toggle="tooltip" data-placement="top" title="Descompartir"><span class="oi oi-x"></span></button>
                    <input type="hidden" name="id_nota" value="<?php echo $nota['id_nota']; ?>" form="descompartir">
                    <input type="hidden" name="id_user" value="<?php echo $_SESSION['id']; ?>" form="descompartir">
                    <input type="hidden" name="origen" value="<?php echo ($row['tipo'] == 3 ? "AN" : "AG"); ?>" form="eliminar">
                  </div>
                <?php endif?>
                <form id="eliminar" action="../controladores/eliminar_nota.php" method="POST"></form>
                <form id="descompartir" action="../controladores/descompartir_nota.php" method="POST"></form>
                <div class="col-auto pr-1 pl-1">
                  <span data-toggle="modal" data-target="#shareModal" data-note-title="<?php echo $nota['titulo']; ?>">
                    <a href="#" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Compartir nota"><span class="oi oi-share-boxed oi-align-center"></span></a>
                  </span>
                </div>
                <div class="col-auto pl-1">
                  <a href="user-overview.php" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Regresar"><span class="oi oi-arrow-left"></span></a>
                </div>
              </div>
            </div>
            <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>E-mail</th>
                    <th>Opciones</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  $consultar = mysqli_query($conn, "SELECT * FROM usuarios WHERE id_user in(SELECT id_user FROM nota_usuario WHERE id_nota='$nota[id_nota]')");
                  // echo mysqli_error($conn);
                  $num_total = mysqli_num_rows($consultar);
                  $tamaño_pagina = 5;
                  //examinar la paginala mostrar y el inicio del registro a mostrar
                  $pagina = $_GET['pagina'];
                  if(!$pagina)
                  {
                    $inicio = 0;
                    $pagina = 1;
                  }
                  else
                  {
                    $inicio = ($pagina - 1) * $tamaño_pagina;
                  }
                  //calculo total de paginas
                  $total_paginas = ceil($num_total / $tamaño_pagina);
                  $consulta = "SELECT * FROM usuarios WHERE id_user in(SELECT id_user FROM nota_usuario WHERE id_nota='$nota[id_nota]') ORDER BY id_user ASC LIMIT ".$inicio."," . $tamaño_pagina;
                  $result = mysqli_query($conn, $consulta);
                  /*if(!$result){
                    echo mysqli_error($conn);
                  }*/
                  if (mysqli_num_rows($result) > 0) 
                  {
                    while ($res = mysqli_fetch_assoc($result))
                    {    
                      if($nota["autor"] != $_SESSION["id"])
                      {
                        echo "<tr>";
                        echo '<td>'.$res["nombre"]." ".$res["apeP"]." ".$res["apeM"]."</td>";
                        echo "<td> ".$res["mail"]."</td>";
                        echo "</tr>";
                      }
                      else
                      {
                        echo "<tr>";
                        echo '<td>'.$res["nombre"]." ".$res["apeP"]." ".$res["apeM"]."</td>";
                        echo "<td> ".$res["mail"]."</td>";
                        //echo "<td><a class='btn btn-danger' href='../controladores/eliminar_grupo.php?id=".$_GET["id"]."&user=".$res["id_user"]."' aria-label='Delete'>Eliminar<i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                        echo '<td><button class="btn btn-sm btn-danger" aria-label="Delete" form="descompartir">Eliminar</button></td>';
                        echo '<input type="hidden" name="id_user" value="'.$res["id_user"].'" form="descompartir">';
                        echo '<input type="hidden" name="id_nota" value="'.$nota['id_nota'].'" form="descompartir">';
                        echo '<input type="hidden" name="origen" value="'.$res['origen'].'" form="descompartir">';
                        echo "</tr>";
                      }
                    }
                  }
                  ?>
                </tbody>
              </table>
              <hr>

              <ul class="pagination justify-content-center">
                <?php
                  if ($total_paginas > 1)
                  {
                    for ($i = 1; $i <= $total_paginas; $i++)
                    {
                      if ($pagina == $i)
                      {
                        //si muestro el índice de la página actual, no coloco enlace
                        echo '<li class="page-item active"><a href="'.$url.'?id='.$_GET["id"].'&?pagina='.$i.'" class="page-link">'.$pagina.'</a></li>';
                        //echo " ".$pagina;
                      }
                      else
                      {
                        //si el índice no corresponde con la página mostrada actualmente,
                        //coloco el enlace para ir a esa página
                        echo '  <li class="page-item"><a href="'.$url.'?id='.$_GET["id"].'&pagina='.$i.'" class="page-link">'.$i.'</a></li>';
                      }
                    }
                  }
                ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- #################### Modals #################### -->
  <?php include '../templates/modal.php'; ?>
  <?php include '../templates/shareModal.php'; ?>

  <!-- Javascript files -->
  <script src="../js/jquery-3.3.1.slim.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <!-- Bootstrap's navbar dependency -->
  <script src="../bootstrap/js/util.js"></script>
  <script src="../bootstrap/js/collapse.js"></script>
  <script src="../bootstrap/js/dropdown.js"></script>
  <script src="../bootstrap/js/modal.js"></script>
  <script src="../bootstrap/js/tooltip.js"></script>
  <script>
    // Pequeño script para activar los tooltips:
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });

    // Modales:
    // Modal para compartir:
    $('#shareModal').on('show.bs.modal', function (event) {
      var modal = $(this);
      var span = $(event.relatedTarget);
      // const noteId = span.data('noteId');
      const noteTitle = span.data('noteTitle');
      modal.find('.modal-title').text(`Compartir "${noteTitle}"`);
      modal.find('.btn-primary').bind('click', function () {
        // Creamos un AJAX y mandamos un POST que elimine la nota:
        const user = modal.find('input[name="usuario"]').val();
        const origin = modal.find('input[name="origen"]').val();
        const values = `nombre=${noteTitle}&usuario=${user}&origen=${origin}`;
        const xhr = new XMLHttpRequest();
        console.log(values);
        xhr.open('POST', '../controladores/compartir_nota.php', true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        xhr.onload = function () {
          if (xhr.status === 200) {
            alert('Nota compartida con éxito!');
          } else {
            alert('Ocurrió un error...');
          }
        };
        xhr.send(values);
        $('#shareModal').modal('toggle');
        window.location.reload();
      });
    });
  </script>
</body>
</html>