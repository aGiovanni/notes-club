<?php 
  require "../controladores/conexion.php";
  require_once "../controladores/obtener_usuario.php";
  $row = getUser($conn);
  if (!$row && $row['tipo'] != 2) {
    ?>
      <script>
        alert("Está intentando acceder sin autorización!");
        window.location.replace("../index.html");
      </script>
    <?php
  } else {
    // Traemos los grupos que tenga el maestro:
    $contenido = mysqli_query($conn, "SELECT * FROM grupos WHERE admin='$_SESSION[id]'");
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Nueva aviso</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css" />
  <link rel="stylesheet" href="../css/open-iconic-bootstrap.min.css">
  <link rel="stylesheet" href="../css/quill.snow.css">
  <link rel="Shortcut Icon" href="../favicon.ico" type="image/x-icon" />
</head>
<body class="navbar-separator bg-light">
  <!-- #################### Navbar #################### -->
  <?php require '../templates/navbar.php' ?>

  <!-- #################### Contenido principal #################### -->
  <section class="separator">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-10">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-auto mr-auto mt-1">
                  <h5 class="mb-0">Nuevo aviso</h5>
                </div>
                <div class="col-auto">
                    <a href="user-overview.php" class="btn btn-primary"><span class="oi oi-arrow-left mr-1"></span>Regresar</a>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form action="../controladores/crear_aviso.php" method="POST" class="needs-validation" novalidate>
                <div class="form-group">
                  <label for="noteTitleInput">Título del aviso</label>
                  <input type="text" class="form-control form-control-lg" name="titulo" id="noteTitleInput" placeholder="Título..." required>
                  <div class="invalid-feedback">
                    Por favor, ingrese algo en el título del aviso.
                  </div>
                </div>  
                <div class="form-group">
                  <label for="tagsInput">Grupo:</label>
                  <select name="id_grupo" class="form-control">
                    <?php while ($grupo = mysqli_fetch_assoc($contenido)): ?>
                      <option value="<?php echo $grupo['id_grupo']; ?>"><?php echo $grupo['nombre_grupo']; ?></option>
                    <?php endwhile ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="output">Aviso:</label>
                  <input name="output" type="hidden">
                  <div id="editor" style="height: 320px;"></div>
                </div>
                <div class="form-group">
                  <input type="hidden" name="origen" value="AG">
                  <button class="btn btn-success btn-lg btn-block" type="submit">Guardar aviso</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- #################### Footer #################### -->
  <footer class="page-footer">
    <div class="footer-copyright">&#x00a9; 2018. NotesClub, Inc.</div>
  </footer>

  <!-- Javascript files -->
  <script src="../js/jquery-3.3.1.slim.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <!-- Bootstrap's navbar dependency -->
  <script src="../bootstrap/js/util.js"></script>
  <script src="../bootstrap/js/collapse.js"></script>
  <script src="../bootstrap/js/dropdown.js"></script>
  <!-- Quill - Text editor -->
  <script src="../js/formValidation.js"></script>
  <script src="../js/quill.min.js"></script>
  <script>
  // Extraemos Delta (Para la manipulación de texto) de quill:
  var Delta = Quill.import('delta');
  // Inicializamos el editor de texto
  var quill = new Quill('#editor', {
    theme: 'snow'
  });
  
  // Comprobamos si existe informacion en LocalStorage
  let context;
  if (localStorage.getItem('texto') === null) {
    context = [];
  } else {
    context = JSON.parse(localStorage.getItem('texto'));
    quill.setContents(context);
  }

  // Guardamos los cambios:
  var change = new Delta();
  quill.on('text-change', function (delta) {
    change = quill.getContents();
  });

  // Guardamos periodicamente:
  setInterval(function () {
    if (change.length() > 0) {
      /* Aqui es donde podemos guardar informacion al server */
      localStorage.setItem('texto', JSON.stringify(change));
      // console.log('Guardando cambios...', change);
      change = new Delta();
    }
  }, 3 * 1000);

  // Buscamos por datos sin guardar
  window.onbeforeunload = function () {
    if (change.length() > 0) {
      return 'Hay cambios sin guardar. ¿Estás seguro de que desea salir de la página?';
    } else {
      if (performance.navigation.type == 2) {
        localStorage.clear();
      }
    }
  }

  // Agregamos el contenido del editor de texto al formulario
  const form = document.querySelector('form');
  form.onsubmit = function (event) {
    const about = document.querySelector('input[name=output]');
    // Comprobamos que exista contenido en el editor
    if (quill.getLength() > 1) {
      // about.value = JSON.stringify(quill.getContents());
      about.value = quill.root.innerHTML;
    } else {
      alert('Por favor, ingrese algo en el campo de texto');
      event.preventDefault();
    }
    // console.log("Formulario enviado!", about.value);
  }
  </script>
</body>
</html>