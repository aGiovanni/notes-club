<?php 
  require "../controladores/conexion.php";
  require_once "../controladores/obtener_usuario.php";
  $row = getUser($conn);
  if (!$row && $row['tipo'] != 2) {
    ?>
      <script>
        alert("Está intentando acceder sin autorización!");
        window.location.replace("../index.html");
      </script>
    <?php
  } else {
    // Traemos los grupos que tenga el maestro:
    if (isset($_GET["id"]) && !empty($_GET["id"])) {
      $contenido = mysqli_query($conn, "SELECT * FROM grupos WHERE id_grupo='$_GET[id]' AND grupos.admin='$_SESSION[id]'");
      if (mysqli_num_rows($contenido) === 1) {
        $grupo = mysqli_fetch_assoc($contenido);
      } else {
        $grupo = [];
      }
    } else {
      ?>
      <script>
        alert("No se pudo encontrar el grupo...");
        window.location.replace("user-overview.php");
      </script>
      <?php
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Grupos</title>
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css" />
  <link rel="stylesheet" href="../css/open-iconic-bootstrap.min.css">
  <link rel="stylesheet" href="../css/quill.snow.css">
  <link rel="Shortcut Icon" href="../favicon.ico" type="image/x-icon" />
</head>
<body class="navbar-separator bg-light">
  <!-- #################### Navbar #################### -->
  <?php require '../templates/navbar.php' ?> 

  <!-- #################### Main Content #################### -->
  <section class="separator">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-10">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-auto mr-auto mt-1">
                  <h5>Grupo: <?php echo $grupo['nombre_grupo']; ?></h5>
                </div>
                <div class="col-auto pr-1 pl-1">
                  <a href="../controladores/borrar_grupo.php?id=<?php echo $grupo['id_grupo']; ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar grupo"><span class="oi oi-trash oi-align-center"></span></a>
                </div>
                <div class="col-auto pr-1 pl-1">
                  <span data-toggle="modal" data-target="#shareModal" data-admin-group="<?php echo $row['id_user']; ?>" data-group-name="<?php echo $grupo['nombre_grupo']; ?>">
                    <a href="#" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Agregar alumno"><span class="oi oi-person oi-align-center"></span></a>
                  </span>
                </div>
                <div class="col-auto pl-1">
                  <a href="user-shared-groups.php" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Regresar"><span class="oi oi-arrow-left"></span></a>
                </div>
              </div>
            </div>

            <div class="card-body">
              <div class="row">
                <div class="col">
                  <div class="table-responsive">
                    <table class="table table-stripped">
                      <thead>
                        <tr>
                          <th>Nombre</th>
                          <th>Correo</th>
                          <th>Opciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $consultar = mysqli_query($conn,"SELECT * FROM usuarios WHERE id_user in(SELECT id_user FROM usuario_grupo WHERE id_grupo=(SELECT id_grupo FROM grupos WHERE id_grupo='$_GET[id]'))");
                          if (mysqli_num_rows($consultar) > 0) {
                            $num_total = mysqli_num_rows($consultar);
                            $tamaño_pagina = 4;
                            //examinar la paginala mostrar y el inicio del registro a mostrar
                            $pagina = $_GET['pagina'];
                            if(!$pagina)
                            {
                              $inicio=0;
                              $pagina=1;
                            }
                            else
                            {
                              $inicio=($pagina - 1) * $tamaño_pagina;
                            }
                            //calculo total de paginas
                            $total_paginas=ceil($num_total / $tamaño_pagina);
                            $consulta = "SELECT * FROM usuarios WHERE id_user in(SELECT id_user FROM usuario_grupo WHERE id_grupo=(SELECT id_grupo FROM grupos WHERE id_grupo='$_GET[id]')) ORDER BY id_user ASC LIMIT ".$inicio."," . $tamaño_pagina;
                            $result=mysqli_query($conn,$consulta);
                            if (mysqli_num_rows($result) > 0) 
                            {
                              while ($res = mysqli_fetch_assoc($result))
                                {
                                  echo "<tr>";
                                  echo '<td>'.$res["nombre"]." ".$res["apeP"]." ".$res["apeM"]."</td>";
                                  echo "<td> ".$res["mail"]."</td>";
                                  echo "<td><a class='btn btn-sm btn-danger' href='../controladores/eliminar_grupo.php?id=".$_GET["id"]."&user=".$res["id_user"]."'>Eliminar</a></td>";
                                  echo "</tr>";
                                }
                            }
                          }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                <div class="col">
                  <ul class="pagination justify-content-center">
                    <?php
                      if ($total_paginas > 1)
                      {
                        for ($i = 1; $i <= $total_paginas; $i++)
                        {
                          if ($pagina == $i)
                          {
                            //si muestro el índice de la página actual, no coloco enlace
                            echo '<li class="page-item active"><a href="'.$url.'?id='.$_GET["id"].'&?pagina='.$i.'" class="page-link">'.$pagina.'</a></li>';
                            //echo " ".$pagina;
                          }
                          else
                          {
                            //si el índice no corresponde con la página mostrada actualmente,
                            //coloco el enlace para ir a esa página
                            echo '  <li class="page-item"><a href="'.$url.'?id='.$_GET["id"].'&pagina='.$i.'" class="page-link">'.$i.'</a></li>';
                          }
                        }
                      }
                    ?>
                  </ul>
                </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>  
  </section>

  <!-- #################### Modals #################### -->
  <?php include '../templates/modal.php'; ?>
  <?php include '../templates/shareModal.php'; ?>

  <!-- Javascript files -->
  <script src="../js/jquery-3.3.1.slim.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <!-- Bootstrap's navbar dependency -->
  <script src="../bootstrap/js/util.js"></script>
  <script src="../bootstrap/js/collapse.js"></script>
  <script src="../bootstrap/js/dropdown.js"></script>
  <script src="../bootstrap/js/modal.js"></script>
  <script src="../bootstrap/js/tooltip.js"></script>
  <script>
  // Pequeño script para activar los tooltips:
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });

  // Modales:
  // Modal para eliminar grupo:
  // Pequeño script para cambiar el contenido del modal:
  $('#confirmModal').on('show.bs.modal', function (event) {
    //Cambiamos el contenido del modal:
    var modal = $(this);
    var span = $(event.relatedTarget);
    const groupId = span.data('groupId');
    const groupTitle = span.data('groupTitle');
    modal.find('.modal-title').text(`Eliminar "${groupTitle}"`);
    modal.find('.modal-body').text('¿Estás seguro de eliminar este grupo?');
    modal.find('.btn-danger').bind('click', function (e) {
      // Creamos un AJAX y mandamos un POST que elimine la nota:
      const xhr = new XMLHttpRequest();
      const values = `id=${groupId}`;
      xhr.open('POST', '../controladores/borrar_grupo.php', true);
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
      xhr.onload = function () {
        if (xhr.status === 200) {
          alert('Grupo eliminado con éxito!');
        } else {
          alert('Ocurrió un error...');
        }
      };
      xhr.send(values);
      $('#confirmModal').modal('toggle');
      window.location.replace('user-shared.groups.php');
    });
  });

  // Modal para compartir:
  $('#shareModal').on('show.bs.modal', function (event) {
    var modal = $(this);
    var span = $(event.relatedTarget);
    const adminId = span.data('adminGroup');
    const groupName = span.data('groupName');
    modal.find('.modal-title').text('Agregar usuario:');
    modal.find('label[for="usrname"]').html('<span class="oi oi-Perso"></span> Correo del usuario: ');
    modal.find('input#usrname').attr('placeholder', 'Nombre del grupo...');
    modal.find('input#usrname').bind('keydown', function (e) {
      if (e.keyCode == 13) {
        e.preventDefault();
      }
    });
    modal.find('.btn-primary').bind('click', function () {
      // Creamos un AJAX y mandamos un POST que elimine la nota:
      const user = modal.find('input#usrname').val();
      const values = `nombre=${groupName}&usuario=${user}&admin=${adminId}`;
      const xhr = new XMLHttpRequest();
      console.log(values);
      xhr.open('POST', '../controladores/agregar_grupo.php', true);
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
      xhr.onload = function () {
        if (xhr.status === 200) {
          alert('Alumno agregado con éxito!');
        } else {
          alert('Ocurrió un error...');
        }
      };
      xhr.send(values);
      $('#shareModal').modal('toggle');
      window.location.reload();
    });
  });
  </script>
</body>
</html>