<?php 
  require "../controladores/conexion.php";
  require_once "../controladores/obtener_usuario.php";
  $row = getUser($conn);
  if (!$row && $row['tipo'] != 2) {
    ?>
      <script>
        alert("Está intentando acceder sin autorización!");
        window.location.replace("../index.html");
      </script>
    <?php
  } else {
    // Traemos los grupos que tenga el maestro:
    $grupos = mysqli_query($conn,"SELECT * FROM grupos WHERE admin='$_SESSION[id]'");
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Grupos</title>
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css" />
  <link rel="stylesheet" href="../css/open-iconic-bootstrap.min.css">
  <link rel="stylesheet" href="../css/quill.snow.css">
  <link rel="Shortcut Icon" href="../favicon.ico" type="image/x-icon" />
</head>
<body class="navbar-separator bg-light">
  <!-- #################### Navbar #################### -->
  <?php require '../templates/navbar.php' ?> 

  <!-- #################### Main Content #################### -->
  <section class="separator">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-10">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-auto mr-auto mt-1">
                  <h5>Grupos:</h5>
                </div>
                <div class="col-auto pr-1 pl-1">
                  <span data-toggle="modal" data-target="#shareModal" data-admin-group="<?php echo $row['id_user']; ?>">
                    <a href="#" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Agregar Grupo"><span class="oi oi-people oi-align-center"></span></a>
                  </span>
                </div>
                <div class="col-auto pl-1">
                  <a href="user-overview.php" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Regresar"><span class="oi oi-arrow-left"></span></a>
                </div>
              </div>
            </div>

            <div class="card-body">
              <?php
                $consulta = mysqli_query($conn, "SELECT * FROM grupos WHERE grupos.admin='$row[id_user]'");
              ?>
              <?php if (mysqli_num_rows($consulta) > 0): ?>
                <?php while ($grupo = mysqli_fetch_assoc($consulta)): ?>
                  <div class="card mb-2">
                    <div class="card-body p-2">
                      <div class="row">
                        <div class="col-auto mr-auto mt-2 ml-1">
                          <h5><a href="user-shared-group.php?id=<?php echo $grupo['id_grupo'];?>"><?php echo $grupo['nombre_grupo']; ?></a></h5>
                        </div>
                        <div class="col-auto mt-1 ml-1">
                          <span data-toggle="modal" data-target="#confirmModal" data-group-id="<?php echo $grupo['id_grupo']; ?>" data-group-title="<?php echo $grupo['nombre_grupo']; ?>">
                            <button class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar grupo"><span class="oi oi-trash"></span></button>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endwhile?>
              <?php endif?>
            </div>

          </div>
        </div>
      </div>
    </div>  
  </section>

  <!-- #################### Modals #################### -->
  <?php include '../templates/modal.php'; ?>
  <?php include '../templates/shareModal.php'; ?>

  <!-- Javascript files -->
  <script src="../js/jquery-3.3.1.slim.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <!-- Bootstrap's navbar dependency -->
  <script src="../bootstrap/js/util.js"></script>
  <script src="../bootstrap/js/collapse.js"></script>
  <script src="../bootstrap/js/dropdown.js"></script>
  <script src="../bootstrap/js/modal.js"></script>
  <script src="../bootstrap/js/tooltip.js"></script>
  <script>
  // Pequeño script para activar los tooltips:
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });

  // Modales:
  // Modal para eliminar grupo:
  // Pequeño script para cambiar el contenido del modal:
  $('#confirmModal').on('show.bs.modal', function (event) {
    //Cambiamos el contenido del modal:
    var modal = $(this);
    var span = $(event.relatedTarget);
    const groupId = span.data('groupId');
    const groupTitle = span.data('groupTitle');
    modal.find('.modal-title').text(`Eliminar "${groupTitle}"`);
    modal.find('.modal-body').text('¿Estás seguro de eliminar este grupo?');
    modal.find('.btn-danger').bind('click', function (e) {
      // Creamos un AJAX y mandamos un POST que elimine la nota:
      const xhr = new XMLHttpRequest();
      const values = `id=${groupId}`;
      xhr.open('POST', '../controladores/borrar_grupo.php', true);
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
      xhr.onload = function () {
        if (xhr.status === 200) {
          alert('Grupo eliminado con éxito!');
        } else {
          alert('Ocurrió un error...');
        }
      };
      xhr.send(values);
      $('#confirmModal').modal('toggle');
      window.location.reload();
    });
  });

  // Modal para compartir:
  $('#shareModal').on('show.bs.modal', function (event) {
    var modal = $(this);
    var span = $(event.relatedTarget);
    const user = span.data('adminGroup');
    modal.find('.modal-title').text('Agregar grupo:');
    modal.find('label[for="usrname"]').html('<span class="oi oi-people"></span> Crear grupo');
    modal.find('input#usrname').attr('placeholder', 'Nombre del grupo...');
    modal.find('input#usrname').bind('keydown', function (e) {
      if (e.keyCode == 13) {
        e.preventDefault();
      }
    });
    modal.find('.btn-primary').bind('click', function () {
      // Creamos un AJAX y mandamos un POST que elimine la nota:
      const group = modal.find('input#usrname').val();
      const values = `nombre=${group}&admin=${user}`;
      const xhr = new XMLHttpRequest();
      console.log(values);
      xhr.open('POST', '../controladores/crear_grupo.php', true);
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
      xhr.onload = function () {
        if (xhr.status === 200) {
          alert('Grupo creado con éxito!');
        } else {
          alert('Ocurrió un error...');
        }
      };
      xhr.send(values);
      $('#shareModal').modal('toggle');
      window.location.reload();
    });
  });
  </script>
</body>
</html>