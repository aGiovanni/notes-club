<?php 
  // header('Content-Type: text/html; charset=ISO-8859-1');
  include "../controladores/conexion.php";
  require_once "../controladores/obtener_usuario.php";
  $row = getUser($conn);
  if (!$row) {
    ?>
      <script>
        alert("Está intentando acceder sin autorización!");
        window.location.replace("../index.html");
      </script>
    <?php
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Administrador</title>
  <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css" />
  <link rel="stylesheet" href="../css/open-iconic-bootstrap.min.css">
  <link rel="stylesheet" href="../css/quill.snow.css">
  <link rel="Shortcut Icon" href="../favicon.ico" type="image/x-icon" />
</head>
<body class="navbar-separator">
  <!-- #################### Navbar #################### -->
  <?php require '../templates/navbar.php'; ?>

  <section class="separator">
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link active" id="v-pills-user-tab" data-toggle="pill" href="#v-pills-user" role="tab" aria-controls="v-pills-user" aria-selected="true">Usuarios</a>
            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Grupos</a>
          </div>
        </div>

        <div class="col-9">
          <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-user" role="tabpanel" aria-labelledby="v-pills-user-tab">
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="user-view-tab" data-toggle="tab" href="#user-view" role="tab" aria-controls="user-view" aria-selected="false">Alumnos</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="teacher-view-tab" data-toggle="tab" href="#teacher-view" role="tab" aria-controls="teacher-view" aria-selected="false">Maestros</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="user-register-tab" data-toggle="tab" href="#user-register" role="tab" aria-controls="user-register" aria-selected="true">Altas</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="user-delete-tab" data-toggle="tab" href="#user-delete" role="tab" aria-controls="user-delete" aria-selected="false">Bajas</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="user-modify-tab" data-toggle="tab" href="#user-modify" role="tab" aria-controls="user-modify" aria-selected="false">Modificaciones</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="user-view" role="tabpanel" aria-labelledby="user-view-tab">
                  <div class="row">
                    <div class="col">
                      <?php include '../templates/user-view.php'; ?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="teacher-view" role="tabpanel" aria-labelledby="teacher-view-tab">
                  <div class="row">
                    <div class="col">
                      <?php include '../templates/teacher-view.php'; ?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="user-register" role="tabpanel" aria-labelledby="user-register-tab">
                  <?php include '../templates/user-register.php'; ?>
                </div>
                <div class="tab-pane fade" id="user-delete" role="tabpanel" aria-labelledby="user-delete-tab">
                  <?php include '../templates/user-delete.php'; ?>
                </div>
                <div class="tab-pane fade" id="user-modify" role="tabpanel" aria-labelledby="user-modify-tab">
                  <?php include '../templates/user-modify.php'; ?>
                </div>
              </div>
            </div>

            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
              <h5>Grupos: </h5>
              <hr>
              <div class="row">
                <div class="col">
                  <?php include '../templates/groups-view.php'; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Javascript files -->
  <script src="../js/jquery-3.3.1.slim.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <!-- Bootstrap's navbar dependency -->
  <script src="../bootstrap/js/util.js"></script>
  <script src="../bootstrap/js/collapse.js"></script>
  <script src="../bootstrap/js/dropdown.js"></script>
  <script src="../bootstrap/js/tab.js"></script>
  <!-- end of navbar dependencies -->
  <script src="../js/formValidation.js"></script>
</body>
</html>