<!-- #################### Navbar #################### -->

<nav class="navbar navbar-shadow fixed-top navbar-expand-md navbar-dark bg-primary">
    <!-- Navbar toggler (responsive design) -->
    <button class="navbar-toggler" 
            type="button" 
            data-toggle="collapse" 
            data-target="#navbarToggler" 
            aria-controls="navbarToggler" 
            aria-expanded="false">
      <span class="navbar-toggler-icon"></span>
    </button>
    <!-- Navbar title (brand) -->
    <div class="align-items-center">
      <a href="../index.html" class="navbar-brand"><span class="brand-highlight">Notes</span>Club</a>
    </div>
    <!-- Navbar navigation content -->
    <div class="collapse navbar-collapse" id="navbarToggler">
      <?php if ($row['tipo'] == 3) :?>
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a href="user-overview.php" class="nav-link">Página principal</a>
          </li>
          <li class="nav-item">
            <a href="user-notes.php" class="nav-link">Notas</a>
          </li>
          <li class="nav-item">
            <a href="user-groups.php" class="nav-link">Grupos</a>
          </li>
        </ul> <!-- Navbar navigation ends here -->
      <?php elseif ($row['tipo'] == 2): ?>
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a href="user-overview.php" class="nav-link">Página principal</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">Avisos</a>
          </li>
          <li class="nav-item">
            <a href="user-shared-groups.php" class="nav-link">Grupos</a>
          </li>
        </ul> <!-- Navbar navigation ends here -->
      <?php elseif ($row['tipo'] == 1): ?>
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a href="#" class="nav-link">Administrador</a>
          </li>
        </ul>
      <?php endif?>

      <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" role="button" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="oi oi-person mr-1"></span><?php echo $row["nombre"]; ?></a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a href="../controladores/logout.php" class="dropdown-item"><span class="oi oi-account-logout mr-2"></span>Cerrar sesión</a>
          </div>
        </li>
      </ul>
  </nav> <!-- Navbar ends here -->