<?php
  $consultar = mysqli_query($conn,"SELECT * FROM usuarios WHERE tipo=3");
  $num_total = mysqli_num_rows($consultar);
  $tamaño_pagina = 10;
  //examinar la paginala mostrar y el inicio del registro a mostrar
  $pagina = $_GET['pagina'];
  if(!$pagina)
  {
    $inicio=0;
    $pagina=1;
  }
  else
  {
    $inicio = ($pagina - 1) * $tamaño_pagina;
  }
  //calculo total de paginas
  $total_paginas = ceil($num_total / $tamaño_pagina);
  //$consulta = "SELECT * FROM atleta ORDER BY posicion_general ASC LIMIT ".$inicio."," . $tamaño_pagina;
  $consulta = "SELECT * FROM usuarios  WHERE tipo=3 ORDER BY id_user ASC LIMIT ".$inicio."," . $tamaño_pagina;
  $result = mysqli_query($conn,$consulta);
  ?>

  <?php if (mysqli_num_rows($result) > 0): ?>
    <h5 class="my-4">Alumnos registrados:</h5>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Correo</th>
          </tr>
        </thead>
        <tbody>
          <?php while($row = mysqli_fetch_assoc($result)): ?>
              <tr>
                <td><?php echo $row['id_user']; ?></td>
                <td><?php echo utf8_encode($row['nombre']) . ' ' . utf8_encode($row['apeP']) . ' ' . utf8_encode($row['apeM']); ?></td>
                <td><?php echo $row['mail']; ?></td>
              </tr>
            <?php endwhile?>
        </tbody>
      </table>
    </div>
    <ul class="pagination justify-content-center">
      <?php
        if ($total_paginas > 1)
        {
          for ($i = 1; $i <= $total_paginas; $i++)
          {
            if ($pagina == $i)
            {
              //si muestro el índice de la página actual, no coloco enlace
              echo '<li class="page-item active"><a href="'.$url.'?pagina='.$i.'" class="page-link">'.$pagina.'</a></li>';
              //echo " ".$pagina;
            }
            else
            {
              //si el índice no corresponde con la página mostrada actualmente,
              //coloco el enlace para ir a esa página
              echo '  <li class="page-item"><a href="'.$url.'?pagina='.$i.'" class="page-link">'.$i.'</a></li>';
            }
          }
        }
      ?>
    </ul>
  <?php else: ?>
    <p>No hay usuarios que mostrar...</p>
  <?php endif?>