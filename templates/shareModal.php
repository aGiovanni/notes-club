<!-- Modal for sharing notes -->

<div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="shareModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="shareModalLabel">Placeholder</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <form role="form" action="../controladores/compartir_nota.php" method="POST">
          <div class="form-group">
            <label for="usrname"><span class="oi oi-person mr-1"></span>Correo del usuario:</label>
            <input type="text" class="form-control" id="usrname" placeholder="Correo..." name="usuario">
            <input type="hidden" name="origen" value="<?php echo ($row['tipo'] == 3 ? "AN" : "AG");?>">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary">Compartir</button>
      </div>
    </div>
  </div>
</div>