<h5 class="mt-4">Eliminar usuario</h5>
<hr>
<form action="#" method="POST" class="needs-validation" novalidate>
  <div class="form-group">
    <label for="email-input">Correo electrónico:</label>
      <input name="mail" type="email" class="form-control" placeholder="nombre@ejemplo.com" required>
      <div class="invalid-feedback">
        Por favor, escriba un correo electrónico válido.
      </div>
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-danger btn-block">Eliminar usuario</button>
  </div>
</form>