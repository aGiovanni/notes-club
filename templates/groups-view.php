<?php
  $consultar = mysqli_query($conn,"SELECT grupos.id_grupo, grupos.nombre_grupo, usuarios.nombre, usuarios.apeP, usuarios.apeM FROM grupos INNER JOIN usuarios ON grupos.admin=usuarios.id_user ORDER BY grupos.id_grupo");
  $num_total = mysqli_num_rows($consultar);
  $tamaño_pagina = 10;
  //examinar la paginala mostrar y el inicio del registro a mostrar
  $pagina = $_GET['pagina'];
  if(!$pagina)
  {
    $inicio=0;
    $pagina=1;
  }
  else
  {
    $inicio = ($pagina - 1) * $tamaño_pagina;
  }
  //calculo total de paginas
  $total_paginas = ceil($num_total / $tamaño_pagina);
  //$consulta = "SELECT * FROM atleta ORDER BY posicion_general ASC LIMIT ".$inicio."," . $tamaño_pagina;
  $consulta = "SELECT grupos.id_grupo, grupos.nombre_grupo, usuarios.nombre, usuarios.apeP, usuarios.apeM FROM grupos INNER JOIN usuarios ON grupos.admin=usuarios.id_user ORDER BY grupos.id_grupo ASC LIMIT ".$inicio."," . $tamaño_pagina;
  $result = mysqli_query($conn,$consulta);
  ?>

  <?php if (mysqli_num_rows($result) > 0): ?>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Grupo</th>
            <th>Propietario</th>
          </tr>
        </thead>
        <tbody>
          <?php while($row = mysqli_fetch_assoc($result)): ?>
              <tr>
                <td><?php echo $row['id_grupo']; ?></td>
                <td><?php echo utf8_encode($row['nombre_grupo']); ?></td>
                <td><?php echo utf8_encode($row['nombre'] . ' ' . $row['apeP'] . ' ' . $row['apeM']); ?></td>
              </tr>
            <?php endwhile?>
        </tbody>
      </table>
    </div>
    <ul class="pagination justify-content-center">
      <?php
        if ($total_paginas > 1)
        {
          for ($i = 1; $i <= $total_paginas; $i++)
          {
            if ($pagina == $i)
            {
              //si muestro el índice de la página actual, no coloco enlace
              echo '<li class="page-item active"><a href="'.$url.'?pagina='.$i.'" class="page-link">'.$pagina.'</a></li>';
              //echo " ".$pagina;
            }
            else
            {
              //si el índice no corresponde con la página mostrada actualmente,
              //coloco el enlace para ir a esa página
              echo '  <li class="page-item"><a href="'.$url.'?pagina='.$i.'" class="page-link">'.$i.'</a></li>';
            }
          }
        }
      ?>
    </ul>
  <?php else: ?>
    <p>No hay grupos que mostrar...</p>
  <?php endif?>