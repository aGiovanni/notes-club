<h5 class="mt-4">Modificar usuario</h5>
<hr>
<form action="../controladores/modificaciones_usuarios.php" method="POST" class="needs-validation" novalidate>
  <div class="form-group">
    <label for="name-input">Nombre:</label>
    <input name="nombre" type="text" class="form-control" placeholder="Nombre..." required>
    <div class="invalid-feedback">
      Por favor, escriba un nombre.
    </div>
  </div>

  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="first-surname-input">Apellido paterno:</label>
      <input name="apeP" type="text" class="form-control" placeholder="Apellido paterno..." required>
      <div class="invalid-feedback">
        Por favor, escriba un apellido paterno.
      </div>
    </div>
    <div class="col-md-6 mb-3">
      <label for="second-surname-input">Apellido materno:</label>
      <input name="apeM" type="text" class="form-control" placeholder="Apellido materno..." required>
      <div class="invalid-feedback">
        Por favor, escriba un apellido materno.
      </div>
    </div>
  </div>

  <div class="form-group">
    <label for="user-type">Tipo de cuenta:</label>
    <select name="tipo" class="form-control">
      <option selected value="3">Alumno</option>
      <option value="2">Docente</option>
    </select>
  </div>

  <div class="form-group">
    <label for="email-input">Correo electrónico:</label>
    <input name="mail" type="email" class="form-control" placeholder="nombre@ejemplo.com" required>
    <div class="invalid-feedback">
      Por favor, escriba un correo electrónico válido.
    </div>
  </div>

  <div class="form-group">
    <label for="password-input">Contraseña:</label>
    <input name="pass" type="password" class="form-control" placeholder="Contraseña..." required>
    <div class="invalid-feedback">
      Por favor, escriba una contraseña.
    </div>
  </div>
  
  <div class="form-group">
    <button type="submit" class="btn btn-success btn-block">Realizar cambios</button>
  </div>
</form>