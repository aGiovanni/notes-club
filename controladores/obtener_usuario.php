<?php
  /** 
   * Retorna el usuario que se encuentra en la sesión
   * @param array $conn
   * @return array $row
   */
  function getUser($conn)
  {
    // reanuda la sesion actual
    session_start();
    // Comprobamos si existe una sesión, en dado caso que no, botamos a la página de inicio
    if (isset($_SESSION['id']) && !empty($_SESSION['id']))
    {
      // trae de la base de datos el nombre del alumno
      $consulta = "SELECT
                    id_user, nombre, tipo
                  FROM
                    usuarios
                  WHERE
                    id_user='$_SESSION[id]'";
      $resultado = mysqli_query($conn, $consulta);
      if (mysqli_num_rows($resultado) > 0)  {
        // output data of each row
        $row = mysqli_fetch_assoc($resultado);
      }
      // Notificamos los errores, a excepción de E_NOTICE
      error_reporting(E_ALL ^ E_NOTICE);
      return $row;
    }
    else {
      return null;
    }
  }
?>