<?php
	session_start();
	//retira todas las variables de sesion
	session_unset();
	//destruye la sesion
	session_destroy();
	header("location: ../index.html");
?>