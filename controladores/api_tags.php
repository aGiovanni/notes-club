<?php
include "../controladores/conexion.php";
$idnota=$_GET['idnota'];

    $array = [];
    $stmt = $conn->prepare(' SELECT tags.tag FROM ((nota_tag INNER JOIN notas ON notas.id_nota = nota_tag.id_nota) INNER JOIN tags ON tags.id_tag = nota_tag.id_tag) WHERE notas.id_nota = ?');
  	$stmt->bind_param('s', $idnota);
  	$stmt->execute();
  	$result = $stmt->get_result();
  	if (mysqli_num_rows($result) > 0)  
  	{
	    while ($row = mysqli_fetch_array($result)) 
	    {
	       array_push($array, $row['tag']);
	    }
  	}

    //RETURN JSON ARRAY
    echo json_encode ($array);
    

?>